/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

/**
 *
 * @author VuXuanThe
 */
public class PresenterCalculator implements IPresenterCalculator{
    private CallBackViewCalculator callBackViewCalculator;
    
    @Override
    public void setView(CallBackViewCalculator backViewCalculator) {
        this.callBackViewCalculator = backViewCalculator;
    }

    @Override
    public void add(double num1, double num2) {
        double answer = num1 + num2;
        if (Double.toString(answer).endsWith(".0")) {
            callBackViewCalculator.updateResult(Double.toString(answer).replace(".0", ""));
        } else {
            callBackViewCalculator.updateResult(Double.toString(answer));
        }
    }

    @Override
    public void sub(double num1, double num2) {
        double answer = num1 - num2;
        if (Double.toString(answer).endsWith(".0")) {
            callBackViewCalculator.updateResult(Double.toString(answer).replace(".0", ""));
        } else {
            callBackViewCalculator.updateResult(Double.toString(answer));
        }
    }

    @Override
    public void mul(double num1, double num2) {
        double answer = num1 * num2;
        if (Double.toString(answer).endsWith(".0")) {
            callBackViewCalculator.updateResult(Double.toString(answer).replace(".0", ""));
        } else {
            callBackViewCalculator.updateResult(Double.toString(answer));
        }
    }

    @Override
    public void div(double num1, double num2) {
        double answer = num1 / num2;
        if (Double.toString(answer).endsWith(".0")) {
            callBackViewCalculator.updateResult(Double.toString(answer).replace(".0", ""));
        } else {
            callBackViewCalculator.updateResult(Double.toString(answer));
        }
    }

    @Override
    public void sqrt(double num) {
        callBackViewCalculator.updateResult(Double.toString(Math.sqrt(num)));
    }

    @Override
    public void setEnable(boolean isEnable) {
        callBackViewCalculator.setEnableCalculator(isEnable);
    }   

    @Override
    public void clear() {
        callBackViewCalculator.clearCalculator();
    }

    @Override
    public void DeleteCharacterInTextHandler(String textHandler) {
        int length = textHandler.length();
            int number = length - 1;
 
 
            if (length > 0) {
                StringBuilder back = new StringBuilder(textHandler);
                back.deleteCharAt(number);
                callBackViewCalculator.updateResult(back.toString());
            }
            if (textHandler.endsWith("")) {
                callBackViewCalculator.updateNumStart("");
            }
    }
}
