/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

/**
 *
 * @author VuXuanThe
 */
public interface IPresenterCalculator {
    public void setView(CallBackViewCalculator backViewCalculator);
    public void setEnable(boolean isEnable);
    public void DeleteCharacterInTextHandler(String textHandler);
    public void clear();
    public void add(double num1, double num2);
    public void sub(double num1, double num2);
    public void mul(double num1, double num2);
    public void div(double num1, double num2);
    public void sqrt(double num);
}
