/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

/**
 *
 * @author VuXuanThe
 */
public interface CallBackViewCalculator {
    public void updateResult(String result);
    public void updateNumStart(String numStart);
    public void setEnableCalculator(boolean isEnable);
    public void clearCalculator();
    public void updateTextHandler(String textHandler);
}
